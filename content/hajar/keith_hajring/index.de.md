+++
title = 'Keith Hajring'
realm = "lebt bei [gnom](https://chaos.social/@gnom)"
marking = "enby-farbenes Freundschaftsband an der Schwanzflosse"
pronouns = "sie/ihr es/ihr"
loves = "kuscheln, schlafen, Bahnreisen"
hates = "alleine sein"
alt = "Keith Hajring liegt in einem Zug auf einem Laptop. Vor dem Fenster ist ein Bahnsteig im Frankfurt Hauptbahnhof zu sehen."
+++

Wenn du mehr von meinen Reisen lesen willst, ich habe auch ein Profil im <a rel="me" href="https://smolhaj.social/@keith_hajring">Fediverse</a> :)

## Bilder

{{< gallery >}}
  <img src="feature_keith_hajring.jpg" class="grid-w33" alt="Keith Hajring im Zug"/>
  <img src="keith_hajring_001.jpg" class="grid-w33" alt="Keith Hajring schaut aus einem Zugfenster. Vor dem Fenster ist eine Baustelle."/>
  <img src="keith_hajring_002.jpg" class="grid-w33" alt="Keith Hajring hängt auf einem Laptop. Der Laptop steht in der Lounge eines ICE3. Die Scheibe nach vorne ist klar geschaltet. Unscharf ist der Blick nach vorne auf die Strecke zu erkennen."/>
  <img src="keith_hajring_003.jpg" class="grid-w33" alt="Keith Hajring kuschelt im Chaosdorf mit Roman und Futura."/>
{{< /gallery >}}
