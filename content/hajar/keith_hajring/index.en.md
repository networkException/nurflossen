+++
title = 'Keith Hajring'
realm = "lives with [gnom](https://chaos.social/@gnom)"
marking = "enby-colored frienschip bracelet on the fin tail"
pronouns = "she/her they/them"
loves = "cuddlling, sleeping, traveling by train"
hates = "being alone"
alt = "Keith Hajring is lying on a laptop in a train at Frankfurt Central."
+++

## images

{{< gallery >}}
  <img src="feature_keith_hajring.jpg" class="grid-w33" alt="Keith Hajring on a train, lying on a laptop."/>
  <img src="keith_hajring_001.jpg" class="grid-w33" alt="Keith Hajring is looking out of a train window. There is a construction site on the outside."/>
  <img src="keith_hajring_002.jpg" class="grid-w33" alt="Keith Hajring is hanging on the display of a laptop. The scene is located in a lounge of an ICE3. The window towards the driver's cabin is transparent and you can see parts of the track ahead."/>
  <img src="keith_hajring_003.jpg" class="grid-w33" alt="Keith Hajring is cuddling with Roman and Futura at the chaosdorf."/>
{{< /gallery >}}
