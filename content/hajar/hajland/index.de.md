+++
title = 'Hajland'
realm = "lebt bei [gim](https://queers.group/@gim)"
pronouns = "leib / ihm"
loves = "alle wie seinen Nächsten"
hates = "diesen Kelch, seinen Vater"
alt = "Der Hajland liegt auf einer Decke und reckt den Kopf nach oben. Im Hintergrund ist ein Fernseher erkennbar."
friends = """
- [#JudasTheCat](https://queer.group/tags/judasthecat)
- [Oktavia the I](https://oktavia.gim.gay/cephalopods/oktavia-i/)
- Hank Seven
"""
+++
