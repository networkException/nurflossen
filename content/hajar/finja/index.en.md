+++
title = 'Finja'
realm = "lives with [gnom](https://chaos.social/@gnom)"
marking = "yellow, red, orange and blue colored frienschip bracelet on the fin tail"
pronouns = "she/her"
loves = "cuddlling, watching gnom at work, watching youtube"
hates = "having to work"
alt = "Finja lying on the bed on orange sheets"
+++

