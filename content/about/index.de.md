+++
title = 'About'
date = 2023-10-11T16:56:28+02:00
draft = false
heroStyle = 'background'
+++

**nur flossen** ist ein privates Projekt.

## Danke

Vielen lieben Dank an [Heatherhorns](https://heatherhorns.com/emoji/), dass ich die Blobhajar auf der Seite benutzen darf

-- BlobHaj von [Heatherhorns](https://heatherhorns.com/) ist lizensiert unter [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/). 

## Wie bekomme ich eine Seite für einen Haj?

### via Merge Request

*Anleitung kommt demnächst*

### Manuell

Du machst ein Foto von deinem Blåhaj.
Das Bild schickst du mir per Mail an haj /a\ nurflossen (.) de, zusammen mit den folgenden Angaben:
- Name des Hajs
- Ein Beschreibungstext für das Bild des Hajs
- Gefilde / Habitat *optional*
- Markierung (Flossenbänden oder so) *optional*
- Pronomen *optional*
- Dinge die dein Haj liebt *optional*
- Dinge die dein Haj hasst *optional*
- Freunde (eine Liste an befreundeten Plushies) *optional*

Gerne kannst du auch noch ein bisschen Text zu deinem Haj schreiben, der dann mit auf die Seite kommt.
